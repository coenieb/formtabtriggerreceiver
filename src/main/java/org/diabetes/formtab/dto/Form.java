package org.diabetes.formtab.dto;

import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Form {
	@Autowired
	private Logger logger;
	
	
	@JsonProperty("_reference")
	private String reference;
	@JsonProperty("_url")
	private String url;
	@JsonProperty("_id")
	private String id;
	@JsonProperty("_device_created")
	private String deviceCreated;
	@JsonProperty("_device_modified")
	private String deviceModified;
	@JsonProperty("_cloud_created")
	private String cloudCreated;
	@JsonProperty("_cloud_modified")
	private String cloudModified;
	@JsonProperty("_username")
	private String username;
	@JsonProperty("_name")
	private String name;
	@JsonProperty("_description")
	private String description;
	@JsonProperty("_status")
	private String status;
	@JsonProperty("_user_ref")
	private String userRef;
	@JsonProperty("_tags")
	private String tags;
	@JsonProperty("_template_name")
	private String templateName;
	@JsonProperty("_pdf")
	private String pdfLink;
	
	
	private Map<String, String> formFields;

	public Form(Map<String, String> values){
		reference = values.remove("_reference");
		url = values.remove("_url");
		id = values.remove("_id");
		deviceCreated = values.remove("_device_created");
		deviceModified = values.remove("_device_modified");
		cloudCreated = values.remove("_cloud_created");
		cloudModified = values.remove("_cloud_modified");
		username = values.remove("_username");
		name = values.remove("_name");
		description = values.remove("_description");
		status = values.remove("_status");
		userRef = values.remove("_user_ref");
		tags = values.remove("_tags");
		templateName = values.remove("_template_name");
		pdfLink =  values.remove("_pdf");
		
		formFields = values;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeviceCreated() {
		return deviceCreated;
	}

	public void setDeviceCreated(String deviceCreated) {
		this.deviceCreated = deviceCreated;
	}

	public String getDeviceModified() {
		return deviceModified;
	}

	public void setDeviceModified(String deviceModified) {
		this.deviceModified = deviceModified;
	}

	public String getCloudCreated() {
		return cloudCreated;
	}

	public void setCloudCreated(String cloudCreated) {
		this.cloudCreated = cloudCreated;
	}

	public String getCloudModified() {
		return cloudModified;
	}

	public void setCloudModified(String cloudModified) {
		this.cloudModified = cloudModified;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserRef() {
		return userRef;
	}

	public void setUserRef(String userRef) {
		this.userRef = userRef;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getPdfLink() {
		return pdfLink;
	}

	public void setPdfLink(String pdfLink) {
		this.pdfLink = pdfLink;
	}

	public Map<String, String> getFormFields() {
		return formFields;
	}

	public void setFormFields(Map<String, String> formFields) {
		this.formFields = formFields;
	}

	
	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			logger.error("{}: {}", e.getClass(), e.getMessage());
		}
		
		return null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(reference, url, templateName, id, pdfLink);
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) 
			return false;
		
		if (obj == this) 
			return true;
		
		if (!(obj instanceof Form)) 
            return false;
		
				
		Form other = (Form) obj;		
		return Objects.equals(reference,other.reference)
				&& Objects.equals(url,other.url)
				&& Objects.equals(templateName,other.templateName)
				&& Objects.equals(id ,other.id)
				&& Objects.equals(pdfLink,other.pdfLink);
	}
	
	

}
