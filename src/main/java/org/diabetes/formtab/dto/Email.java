/**
 * 
 */
package org.diabetes.formtab.dto;

/**
 * @author coenieb
 *
 */
public class Email {
	private String to = "coenie.basson@diabetes.org.uk";
	private String from = "coenie.basson@diabetes.org.uk";
	private String cc;
	private String bcc;
	private String subject = "FormTab Integration testing";
	private String message;
	
	
	public String getTo(){
		return this.to;
	}
	public void setTo(String t){
		this.to = t;
	}
	
	public String getFrom(){
		return this.from;
	}
	public void setFrom(String t){
		this.from = t;
	}
	
	public String getCc(){
		return this.cc;
	}
	
	public void setCc(String t){
		this.cc = t;
	}
	
	public String getBcc(){
		return this.bcc;
	}
	
	public void setBcc(String t){
		this.bcc = t;
	}	
	
	public String getSubject(){
		return this.subject;
	}
	
	public void setSubject(String t){
		this.subject = t;
	}
	
	public String getMessage(){
		return this.message;
	}
	public void setMessage(String t){
		this.message = t;
	}

}
