/**
 * 
 */
package org.diabetes.formtab.utilities;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.diabetes.formtab.dto.Email;
import org.diabetes.formtab.dto.Form;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author coenieb
 *
 */
@Component
public class Mailer {
	@Autowired
	private JavaMailSender javaMailSender;


	public void sendMail(Email emailTemplate) throws MessagingException {
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom(emailTemplate.getFrom());
		if (StringUtils.hasText(emailTemplate.getTo()))
			helper.setTo(StringUtils.delimitedListToStringArray(emailTemplate.getTo(), ";"));
		if (StringUtils.hasText(emailTemplate.getCc()))
			helper.setCc(StringUtils.delimitedListToStringArray(emailTemplate.getCc(), ";"));
		if (StringUtils.hasText(emailTemplate.getBcc()))
			helper.setBcc(StringUtils.delimitedListToStringArray(emailTemplate.getBcc(), ";"));

		helper.setSubject(emailTemplate.getSubject());
		helper.setText(emailTemplate.getMessage(), true);	// use the true flag to indicate the text included is HTML

		javaMailSender.send(message);
	}


	public void sendForm(Form form) throws MessagingException {
		Email emailTemplate = new Email();
		
		emailTemplate.setTo("coenie.basson@diabetes.org.uk");
		emailTemplate.setSubject("Form submitted (ref:"+form.getReference()+")");
		emailTemplate.setMessage(form.toString());
		
		sendMail(emailTemplate);
	}
	

}
