package org.diabetes.formtab.api;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.mail.MessagingException;

import org.apache.logging.log4j.Logger;
import org.diabetes.formtab.dto.Form;
import org.diabetes.formtab.dto.Greeting;
import org.diabetes.formtab.utilities.Mailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ApiController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@Autowired
	private Mailer mailer;
	@Autowired
	private Logger logger;
	@Autowired
	private ObjectMapper mapper;

	@RequestMapping(value="/greeting", method=RequestMethod.GET)
	public @ResponseBody Greeting sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {

		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@RequestMapping(value="/submit-form", method=RequestMethod.POST)
	public @ResponseBody HttpStatus processExampleForm(@RequestBody String jsonString) {
		logger.debug("Received: {}",jsonString);

		try {
			processForm(jsonString);
			return HttpStatus.OK;
		} catch (Exception e) {
			logger.error("{} occurred while processing submit-form: {}", e.getClass(), e.getMessage());
			e.printStackTrace();
		}

		return HttpStatus.BAD_REQUEST;
	}

	/**
	 * @param jsonString
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @throws MessagingException 
	 */
	private void processForm(String jsonString) throws JsonParseException, JsonMappingException, IOException, MessagingException {
		Map<String, String> jsonMap = mapper.readValue(jsonString, new TypeReference<Map<String, String>>(){});
		mailer.sendForm(new Form(jsonMap));
	}

}