package org.diabetes.formtab.config;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@PropertySource ({"classpath:application.properties", "classpath:KiteWorksAPI.properties"})
public class SpringConfig {
	/*
	 *  Imports property files as specified by the @PropertySource annotation
	 */
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
		return new PropertySourcesPlaceholderConfigurer();		
	}

	// Email host properties
	@Value("${mailserver.host}")
	private String mailserverHost;
	@Value("${mailserver.port}")
	private Integer mailserverPort;
//	@Value("${mailserver.username}")
//	private String mailserverUsername;
//	@Value("${mailserver.password}")
//	private String mailserverPassword;
	
	@Bean
	public Logger logger(){
		return LogManager.getLogger();
	}
	
	@Bean
	public RestTemplate restTemplate(){
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		return restTemplate;
	}
	
	
	/*
	 *  Jackson ObjectMapper is used to map objects to and from JSON
	 */
	@Bean
	public ObjectMapper mapper(){
		return new ObjectMapper();
	}
	
	@Bean
	public JavaMailSender javaMailSender(){
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(mailserverHost);
		mailSender.setPort(mailserverPort);
//		mailSender.setUsername(mailserverUsername);
//		mailSender.setPassword(mailserverPassword);
		
		Properties prop = mailSender.getJavaMailProperties();
		prop.put("mail.transport.protocol", "smtp");
		prop.put("mail.smtp.auth", "false");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.debug", "false");
		
		return mailSender;
	}

}
